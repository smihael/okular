# Lithuanian translations for okular_plucker package.
# This file is distributed under the same license as the okular_plucker package.
#
# Andrius Štikonas <andrius@stikonas.eu>, 2008.
# Liudas Ališauskas <liudas.alisauskas@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: okular_plucker\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:27+0000\n"
"PO-Revision-Date: 2011-10-08 11:28+0300\n"
"Last-Translator: Liudas Ališauskas <liudas.alisauskas@gmail.com>\n"
"Language-Team: Lithuanian <kde-i18n-doc@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Lokalize 1.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Andrius Štikonas, Liudas Ališauskas"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "andrius@stikonas.eu, liudas@akmc.lt"

#: generator_plucker.cpp:73
#, kde-format
msgid "Name"
msgstr "Pavadinimas"

#~ msgid "Plucker Document Backend"
#~ msgstr "Plucker dokumentų programinė sąsaja"

#~ msgid "A renderer for Plucker eBooks"
#~ msgstr "Plucker eBooks generatorius"

#~ msgid "© 2007-2008 Tobias Koenig"
#~ msgstr "© 2007-2008 Tobias Koenig"

#~ msgid "Tobias Koenig"
#~ msgstr "Tobias Koenig"
